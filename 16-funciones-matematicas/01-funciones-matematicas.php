<?php 
/**
 * Funciones necesarias para realizar operaciones matematicas
 * no hay mucho que explicar, simplemente debemos usar la
 * funcion adecuada
 * 
 * @link https://www.php.net/manual/en/ref.math.php
 */

# por ejemplo redondear un numero

echo round(11 / 2); // round() redondea para arriba
echo "<br>";
echo floor(11 / 2); // floor() redondea para abajo