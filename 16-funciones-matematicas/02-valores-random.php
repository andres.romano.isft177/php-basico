<?php 
/**
 * Valores random
 * 
 * @link https://www.php.net/manual/en/function.rand
 */

$min = 0;
$max = 100;

echo rand($min, $max);