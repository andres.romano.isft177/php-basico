<?php 
/**
 * Una excepción debe contar con una estructura de tipo try-throw-catch.
 * try - La función que nos podría generar una excepción (o error) 
 * 	debe ir dentro de un bloque "try". Si no se lanza la excepción el 
 * 	código sigue en forma normal. Si el error aparece (excepción)se 
 * 	lanza el bloque throw.
 * throw - Es cuando se lanza la excepción. Cada “throw” deberá 
 * 	tener su "catch" (atrapar).
 * catch - Un bloque “catch" está relacionada con una excepción y crear 
 * 	un objeto que contiene la información del objeto.
 * 
 * @link https://www.php.net/manual/es/class.exception
 */

	function suma($n1, $n2) {
		$suma = $n1+$n2;
		if ($suma>10) {
			throw new Exception("La suma debe ser menor a 10");
		}
		return true;
	}

	try {
		suma(1,5);
		echo "Si vemos esta cadena, es que la suma es menor a 10";

	} catch (Exception $e) {
		echo "Mensaje: ".$e->getMessage();
	}