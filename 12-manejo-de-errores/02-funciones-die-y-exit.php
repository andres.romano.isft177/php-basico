<?php 
/*
Por medio de las funciones die() o exit() podemos terminar un 
programa enviando un mensaje de error.
*/

if(file_exists("datos.txt")) {
#if(file_exists("../_files/datos1.txt")) {
	echo "el archivo si existe<br>";
} else {
	#die("No existe el archivo 'datos.txt', usando die()");
	exit("No existe el archivo 'datos.txt', usando exit()");
}
echo "Este mensaje no se diespliega si hay un error.";