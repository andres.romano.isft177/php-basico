<?php 
/**
 * Por omisión PHP envía errores a pantalla con archivo, número de línea y mensaje.
 * En algunas instalaciones, por ejemplo en MAMP, los errores pueden estar deshabilitados.
 * Podemos habilitarlos con:
 * 
 *  ini_set('display_errors', 1);
 *  error_reporting(E_ALL);
 * 
 * al inicio del codigo
 * 
 * @link https://www.php.net/manual/es/function.error-reporting
 * 
 * @link https://www.php.net/manual/es/function.ini-set
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);