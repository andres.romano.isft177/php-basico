<?php 
/** 
 * Podemos crear nuestras propias funciones a las que se 
 * les llama "funciones de usuario", no siempre reciben 
 * parametros y no siempre es necesario aclarar que 
 * devuelve. El nombre de la funcion al menos debe dar
 * un indicio de que realiza o devuelve, como buena practica 
 * se utiliza camelCase en su nombre.
 * 
 * @link https://www.php.net/manual/es/language.functions.php
 */

$persona = "Andres Romano"; // $persona es variable de contexto global
# Declarar una funcion
function saludar() {
	echo "<p>Esta funcion sirve para saludar.</p>";
}
# Ejecutar una funcion
saludar();

function saludoPersonalizado($nombre) { // $nombre es variable de contexto local
	echo "<p>Esta funcion saluda a, ".$nombre."</p>";
}

saludoPersonalizado($persona);

/*
El alcance de las variables es segun su contexto, y pueden de global o local.
global: se puede utilizar en todo el script, se puede pasar como parametro a una funcion
local: son variables a las que solo se accede dentro de la funcion
para acceder a una varible de contexto global dentro de una funcion, y esta variable 
no fue pasada por parametros se debe utilizar $GLOBALS['variable']
*/
$persona2 = "Romano Andres";

function segundoSaludo($local) {
	echo "<p>variable local ".$local."</p>";
	echo "<p>variable global ".$GLOBALS['persona']."</p>";
}

segundoSaludo($persona2);