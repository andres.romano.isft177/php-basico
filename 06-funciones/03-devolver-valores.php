<?php 
/*
Los valores son devueltos usando la sentencia opcional return. 
Se puede devolver cualquier tipo, incluidos arrays y objetos. 
Esto causa que la función finalice su ejecución inmediatamente 
y pase el control de nuevo a la línea desde la que fue llamada.
*/
# Devolver un string

function verMensaje() {

	return "Mensaje de return";
}

echo verMensaje();

echo "<br><br>";

# Devolver un booleano

function trueOrFalse($numero) {
	return $numero > 0 ? true : false;
}

$num = -5;
if (trueOrFalse($num)) {
	echo "el numero es positivo";
} else {
	echo "el numero es negativo";
}
