<?php 
/*
Cualquier información puede ser pasada a las funciones mediante la lista de 
argumentos, la cual es una lista de expresiones delimitadas por comas. 
Los argumentos son evaluados de izquierda a derecha. PHP también permite 
el uso de arrays y del tipo especial null como valores predeterminados. El valor
predeterminado debe ser una expresión constante, no (por ejemplo) una variable,
un miembro de una clase o una llamada a una función. Cuando se emplean argumentos
predeterminados, cualquiera de ellos debería estar a la derecha de los argumentos
no predeterminados, sino dara algun error

*/
# Pasar un array a funcion

$array = [3,"numero",10,5,"funcion"];

function verArray($array) {

	for ($i=0; $i < count($array); $i++) { 
		echo $array[$i]." ";
	}

}

verArray($array);

echo "<br><br>";

# Funciones con valores predeterminados

function tablaMultiplicar($numero, $multi = [1,2,3,4,5,6,7,8,9,10]) {

	for ($i=0; $i < count($multi); $i++) { 
		echo $numero." X ".$multi[$i]." = ".($numero * $multi[$i])."<br>";
	}
	
}

// si la funcion tiene un argumento predeterminado,no es necesario pasarle un valor
$function = "tablaMultiplicar";
tablaMultiplicar(8);
$function(7);

echo "<br><br>";

function mostrarValores($valor1, $valor2 = null) {

	echo $valor1."<br>";

	echo $valor2 != null ? "el nuevo valor2 es: ".$valor2."<br>" : "valor2 sigue siendo null<br>";

}

mostrarValores("este es valor1");
mostrarValores("este es valor1",5);
mostrarValores("este es valor1","valor5");

