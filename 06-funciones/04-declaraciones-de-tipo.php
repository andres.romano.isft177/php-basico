<?php 
/*
Las declaraciones de tipo permiten a las funciones requerir que los
parámetros sean de cierto tipo durante una llamada. Si el valor dado
es de un tipo incorrecto, se generará un error. Tambien se puede 
especificar que valor devuelve. Para que esto funciones se debe activar
una configuracion de php declare(strict_types=1);

*/
declare(strict_types=1);

function recivirParametro(string $name) {
	echo "parametro recivibo ".$name;
}

recivirParametro("Andres");

echo "<br><br>";

function suma($n1, $n2):bool {
#function suma($n1, $n2):int {
	return $n1+$n2;
}
var_dump(suma(10,5));

echo "<br><br>";

class Gato{};
class Perro{};
function regresaGato(): Gato {
	return new Perro;
	#return new Gato;
}
var_dump(regresaGato());
