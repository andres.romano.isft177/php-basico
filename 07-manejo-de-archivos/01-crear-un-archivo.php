<?php 
/**
 * Desde PHP podemos manipular archivos que tengan que ver con 
 * nuestra aplicacion o bien ficheros de configuracion de
 * fileSystem.
 * 
 * @link https://www.php.net/manual/en/ref.filesystem.php
 */

# crear un archivo

$dir = "../_files/"; // directorio que usaremos de practica
$file = "datos.txt"; // nombre del archivo

touch($dir.$file);

# comprobar si se creo el archivo

if (file_exists($dir.$file)) {
	echo "el archivo se creo correctamente";
}

