<?php 

$dir = "../_files/"; // directorio que usaremos de practica
$file = "datos1.txt"; // nombre del archivo
$bkp = "datos1.txt.bk"; // nombre del archivo

/*
 Copiar un archivo para poder hacer, como por ejemplo, un BKP.
 utilizamos un if para poder informar si se copio o no.
*/

if (copy($dir.$file, $dir.$bkp)) {
	echo "se copio el archivo correctamente";
} else {
	echo "no se pudo copiar el archivo";
}