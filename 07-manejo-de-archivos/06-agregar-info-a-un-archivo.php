<?php 

$dir = "../_files/"; // directorio que usaremos de practica
$file = "datos1.txt"; // nombre del archivo

/* 
 abrir el archivo modo escritura "a" se utiliza para agregar 
 informacion al final del archivo
*/

if (is_writeable($dir.$file)) {

	$archivo = fopen($dir.$file, "a");

	fwrite($archivo, "linea 4"."\n");
	fwrite($archivo, "linea 5"."\n");
	fwrite($archivo, "linea 6"."\n");

	fclose($archivo);

	echo "se escribio en el archivo correctamente";

} else {

	echo "no se puede grabar en el archivo";

}
