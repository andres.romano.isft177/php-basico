<?php 
/*
Importante luego de crear un archivo se le debe dar permisos si es
que tambien lo vamos a utilizar dentro de la aplicacion o pretendemos
modificarlo por fuera del programa, a parte dedarle permisos tambien
podemos especificar el propietario y grupo propietario. Tener en cuenta
desde donde se ejecuta el script php al crear el archivo puede haber un 
conflicto de permisos y tal vez no tengamos control del archivo creado
*/

# dar permisos a nuestro archivo

$dir = "../_files/"; // directorio que usaremos de practica
$file = "datos1.txt"; // nombre del archivo

if (touch($dir.$file)) {
	chmod($dir.$file, 0777);
#	chown($dir.$file, 1000);
#	chgrp($dir.$file, 1000);
}

if (file_exists($dir.$file)) {
	echo "se creo el archivo y se le dieron los permisos correspondientes";
}
