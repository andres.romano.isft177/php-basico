<?php 
/*
Contantes predefinidas, de doble guion bajo, o tambien
se las llama contantes magicas
__LINE__ Número de línea actual en el archivo.
__FILE__ Ruta completa y nombre del archivo. Si se usa dentro de un include, devolverá el
nombre del fichero incluido.
__DIR__ Directorio del archivo. Si se utiliza dentro de un include, devolverá el directorio del
archivo incluído.
declarado.
__METHOD__ Nombre del método de la clase.
__FUNCTION__ Nombre de la función.
__CLASS__ Nombre de la clase.
*/
function prueba(){
	echo "linea n° ".__LINE__." desde dentro de la funcion<br>";
	echo "la funcion se llama ".__FUNCTION__."<br>";
}

prueba();

echo "<br>";

echo "linea n° ".__LINE__." desde fuera de la funcion<br>";
echo __FILE__."<br>";
echo __DIR__."<br>";

echo "<br>";

class Materia {
	public function nombre() {
		echo "clase: ".__CLASS__."<br>";
		echo "metodo: ".__METHOD__."<br>";
	}
}

(new Materia())->nombre();
