<?php 

$dir = "../_files/"; // directorio que usaremos de practica
$file = "datos1.txt"; // nombre del archivo

/* 
 abrir el archivo modo escritura "w" trunca el archivo a 0, y agrega 
 la informacion al principio del archivo y antes de  escribir en 
 el archivo, primero verificar que se tenga permiso.
*/

if (is_writeable($dir.$file)) {

	$archivo = fopen($dir.$file, "w");

	fwrite($archivo, "linea 1"."\n");
	fwrite($archivo, "linea 2"."\n");
	fwrite($archivo, "linea 3"."\n");

	fclose($archivo);

	echo "se escribio en el archivo correctamente";

} else {

	echo "no se puede grabar en el archivo";

}
