<?php 

$dir = "../_files/"; // directorio que usaremos de practica
$file = "datos1.txt"; // nombre del archivo

# recorrerlo y leerlo, comprobar permisos

if (is_readable($dir.$file)) {
	# abrir el archivo modo solo lectura

	$archivo = fopen($dir.$file, "r");

	while (!feof($archivo)) {
		$line = fgets($archivo);
		echo $line."<br>";
	}
	
	fclose($archivo);

}
