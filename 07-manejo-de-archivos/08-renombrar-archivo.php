<?php 

$dir = "../_files/"; // directorio que usaremos de practica
$file = "datos1.txt"; // nombre del archivo
$bkp = "datos1.txt.bk"; // nombre del archivo

/*
 Copiar un archivo para poder hacer, como por ejemplo, un BKP.
 utilizamos un if para poder informar si se copio o no.
*/

if (rename($dir.$bkp, $dir."datos_bk.txt")) {
	echo "se renombro el archivo correctamente";
} else {
	echo "no se pudo renombrar el archivo";
}