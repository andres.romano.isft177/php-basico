<?php 
/*
Utilizando fgetcsv() voy a obtener un array con todas las columnas
del archivo CSV, el orden de los indices del array va a coincidir
con el encabezado del archivo, en este ejemplo seria:

name,surname,email,phone,address 
*/

$csv = "../_files/personas.csv"; // archivo de practica

$columns = true;

if (is_readable($csv)) {
	
	$file = fopen($csv, "r");

	while (!feof($file)) {

		$line = fgetcsv($file);
		/*
		# este bloque excluye la primera linea que es el encabezado
		if ($columns) {
			$columns = false;
			continue;
		}
		*/
		#var_dump($line);
		#echo "<br>";
		#/*
		echo "Nombre completo: ".$line[0]." ".$line[1]."<br>";
		echo "Email: ".$line[2]."<br>";
		echo "Telefono: ".$line[3]."<br>";
		echo "Direccion: ".$line[4]."<br>";
		echo "-------------<br>";
		#*/	

	}

	fclose($file);

}
