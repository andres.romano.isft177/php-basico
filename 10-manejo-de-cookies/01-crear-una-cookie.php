<?php 
/**
 * ¿Que son las Cookies? basicamente es guardar informacion del 
 * usuario en el navegador cliente, puede tener varios usos, como
 * por ejemplo identificar al usuario antes del login o simplemente
 * hacer estadisticas de los sitios que se visitan, dejo un link que
 * explica más a fondo que son y para que se utilizan
 * 
 * @link https://www.xataka.com/basics/que-cookies-que-tipos-hay-que-pasa-desactivas
 * 
 * para crear una cookie, se debe declarar antes de lanzar algo al 
 * navegador, es recomendable separar los datos con |
 */

$nombre = "datos";
$valor = "andres@mail.com|Andres|Romano";
$fecha = time() + (60*60*24); // time() duvuelve hora Unix, (seg*min*horas*dias)
setcookie($nombre, $valor, $fecha);
?>
<!DOCTYPE html>
<html>
<head>
 <title>Cookies | Crear</title>
 <meta charset="utf-8">
</head>
 <body>
 	<h3>Cookies!</h3>
 </body>
</html>