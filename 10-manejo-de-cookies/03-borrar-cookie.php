<?php 
/*
 Para borrar una cookie, la manera más segura es crear la cookie 
 con el mismo nombre, con un valor "vacío" y una fecha de 
 expiración "vencida".
*/
setcookie('datos',"",time()-1);
