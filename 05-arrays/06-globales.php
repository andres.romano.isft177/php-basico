<?php
/**
 * Varias variables predefinidas en PHP son "superglobales", lo que 
 * significa que están disponibles en todos los ámbitos de un script.
 * 
 * Las variables globales son:
 * $_SESSION
 * $_COOKIE
 * $_ENV
 * $_FILES
 * $_GET
 * $_POST
 * $_REQUEST
 * $_SERVER
 * $GLOBALS
 * El arreglo $GLOBALS nos permite utilizar una variable en cualquier parte del programa.
 * 
 * @link https://www.php.net/manual/en/language.variables.superglobals.php
 */

print $_SERVER["PHP_SELF"]."<br>";
print $_SERVER["SERVER_NAME"]."<br>";
print $_SERVER["HTTP_HOST"]."<br>";
print $_SERVER["HTTP_REFERER"]."<br>";
print $_SERVER["SCRIPT_NAME"]."<br>";
print $_SERVER["HTTP_USER_AGENT"]."<br>";

foreach ($_ENV as $key => $value) {
	echo $key."=".$value."<br>";
}