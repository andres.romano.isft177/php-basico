<?php 
/*
Para recorrer array lo mas recomendable es utilizar un ciclo llamado foreach

foreach ($variable as $key => $value) {
	// code...
}
*/

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio",
"Agosto","Septiembre","Octubre","Noviembre","Diciembre");

var_dump($meses);
echo "<br><br>";

foreach ($meses as $key => $value) {
	echo $key." ".$value."<br>";
}

foreach ($meses as $key => $value) {
	echo $value."<br>";
}