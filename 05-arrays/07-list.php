<?php
/*
Al igual que array(), no es realmente una función, es un constructor 
del lenguaje. list() se utiliza para asignar una lista de variables 
en una sola operación. list() solo funciona con arrays numéricos y 
supone que los índices numéricos empiezan en 0.
*/

$info = ['café', 'marrón', 'cafeína'];

// Enumerar todas las variables
list($bebida, $color, $energía) = $info;
echo "El ".$bebida." es ".$color." y la ".$energía." lo hace especial.<br>";

// Enumerar algunas de ellas
list($bebida, , $energía) = $info;
echo "El ".$bebida." tiene ".$energía."<br>";
