<?php 
/*
PHP permite crear arreglos cuyos elementos tienen nombres en vez de números.
No siempre es conveniente usar números para identificar los elementos de un arreglo. 
Para el humano es más fácil usar "letreros" o "nombres" para identificar las cosas.
*/

$horario = [
   "materia" => "bases de datos", 
   "profe" => "andres romano", 
   "dia" => "miercoles",
   "horario" => "20 a 22 hs"
];

foreach ($horario as $key => $value) {
	echo "la key se llama: <b>".$key."</b> y su valor es: <b>".$value."</b><br>";
}