<?php
/*
Podemos combinar arreglos con índices numéricos o asociativos.
*/

$ciudades = array(
	["México","CDMX","Guadalajara","Monterrey"],
	["España","Madrid","Barcelona","Bilbao"],
	["Colombia","Bogotá","Bucaramanga","Medellin"],
	["Perú","Lima","Cuzco","Arequipa", "Chiclayo", "Ayacucho"]
);

for($i = 0; $i < count($ciudades); $i++){
	print "<ul>";
	for($j=0; $j < count($ciudades[$i]); $j++){
		print "<li>".$ciudades[$i][$j]."</li>";
	}
	print "</ul>";
}