<?php 
/**
 * En este archivo se explican algunas funciones de array
 * para tener en cuenta, el link pertenece a la documentacion
 * oficial de PHP donde pueden consultar todas las funciones
 * de array.
 * 
 * @link https://www.php.net/manual/es/ref.array.php
 */

$array = [1, 2.3, "elemento", 8 => "valor", 10 => "10"];
$array1 = ["a" => "andres", "d" => "dario"];

# Buscar por un valor exacto en todo el arreglo, con in_array().

echo "in_array()<br>";
echo in_array(2.3, $array) ? "el valor existe" : "el valor NO existe";
echo "<br><br>";
echo in_array("2.3", $array, true) ? "el valor existe" : "el valor NO existe";
echo "<br><br>";
echo in_array("elemento", $array) ? "el valor existe" : "el valor NO existe";

# Combinar dos o más arrays con array_merge()

echo "<br><br>";
echo "array_merge()<br>";
$array2 = array_merge($array1,$array);
var_dump($array2);

# Agregar elementos a un array con array_push()

echo "<br><br>";
echo "array_push()<br>";
array_push($array1, "papas");
$array1["b"] = "batatas";
echo "<br>";
var_dump($array1);

# Verificar que exista un indice (key)

echo "<br><br>";
echo "array_key_exists()<br>";
echo array_key_exists("b", $array1) ? "la key b existe" : "la key b NO existe";
