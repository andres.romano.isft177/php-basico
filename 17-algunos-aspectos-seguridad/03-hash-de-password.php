<?php 
/**
 * Encriptar y desencriptar contraseñas, hay varios metodos,
 * se utilizaban md5() sha1() hash() crypt(), en este ejemplo 
 * usaremos password_hash()
 * 
 * @link https://www.php.net/manual/en/function.password-hash
 * 
 * y dejo un articulo con info
 * 
 * @link https://diego.com.es/encriptacion-y-contrasenas-en-php
 */

$pass = "password123";

$hash = password_hash($pass, PASSWORD_DEFAULT);

echo "la clave encriptada tiene ".strlen($hash)." caracteres";
echo "<br>";
echo $hash;

echo "<br><br>";

if (password_verify($pass, $hash)) {
	echo "password correcto!!";
}