<?php 
/**
 * Las funciones de base64 sirven para codificar, o decodificar, un string
 * pero no son un metedo de encryptacion, su uso generalmente es para 
 * transportar informacion dentro de la aplicación
 * 
 * @link https://www.php.net/manual/en/function.base64-encode
 * 
 */

# un ejemplo puede ser codificar un id que se expone en la url

$usuario_id = 1;
?>
<a href="http://localhost:8050/php-basico/14-algunos-aspectos-seguridad/02-funciones-base64.php?usuario=<?php echo base64_encode($usuario_id) ?>">usuario</a><br>

<p>y luego podemos decodificarlo</p>

<p>usuario id es: <?php echo base64_decode($_GET['usuario']) ?> </p>

<a href="http://localhost:8050/php-basico/14-algunos-aspectos-seguridad/02-funciones-base64.php">volver</a>

<p>otro ejemplo puede ser en formularios</p>

<form action="" method="post">
	<input type="text" name="user" value="andres" readonly>
	<input type="hidden" name="id" value="<?php echo base64_encode($usuario_id) ?>">
	<input type="submit" name="submit" value="enviar">
</form>
<?php 
if (isset($_POST['submit'])) {
	echo $_POST['user']."<br>";
	echo $_POST['id']."<br>";
}
echo "se actualizaron los datos del usuario: ".$_POST['user']." con id: ".base64_decode($_POST['id']);
