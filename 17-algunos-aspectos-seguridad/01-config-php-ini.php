<?php 
/**
 * El archivo php.ini establece la configuracion de PHP, hay ciertas variables
 * que podemos modificar segun lo que necesitemos, o dependiendo del ambiente
 * en que estemos, si es produccion o si es desarrollo.
 *  
 * El srchivo se encuentra en: /usr/local/etc/php/php.ini
 * 
 * Puntos de revision:
 * 
 * display_errors
 * max_execution_time
 * memory_limit
 * upload_max_filesize
 * file_upload
 * upload_tmp_dir
 * allow_url_include
 * 
 * @link https://www.php.net/manual/en/ini.core.php
 * 
 */
