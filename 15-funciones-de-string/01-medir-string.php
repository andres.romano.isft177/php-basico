<?php 
/**
 * Funciones de string
 * 
 * @link https://www.php.net/manual/es/ref.strings.php
 * 
 * Texto de relleno
 * 
 * @link https://es.lipsum.com/
 */

$string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

echo $string;
echo "<br>";
echo "El texto de relleno tiene ".strlen($string)." carateres";
