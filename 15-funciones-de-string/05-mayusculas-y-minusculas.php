<?php 
/*
  Convertir todo a mayusculas o minusculas strtolower() 
  y strtoupper(), la primera letra de cada palabra 
  ucwords(), ó, solo la primera letra ucfirst()
*/
$string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

echo strtoupper($string);
echo "<br><br>";
echo strtolower($string);
echo "<br><br>";
echo ucwords($string);
echo "<br><br>";
echo ucfirst($string);
echo "<br><br>";

