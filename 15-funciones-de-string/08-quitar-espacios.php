<?php 
/*
  Utilizamos trim() - ltrim() - rtrim(), para quitar espacios 
  en blanco de las cadenas, "l" y "r" hacen referencia
  a right y left
*/
$string = "            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.                ";

echo "<pre>".$string."</pre>";
echo "<br><br>";
echo "<pre>".trim($string)."</pre>";
echo "<br><br>";
echo "<pre>".ltrim($string)."</pre>";
echo "<br><br>";
echo "<pre>".rtrim($string)."</pre>";
echo "<br><br>";