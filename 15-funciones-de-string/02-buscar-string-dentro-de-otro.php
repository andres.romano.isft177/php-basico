<?php 
/*
  Buscar un string dentro de otro con stristr() — strstr()
  las 2 funciones hacen los mismo, buscan un string y devuevle
  todo el restro del string partiendo de donde encontro, solo que 
  la "i" significa "insensitive", hace referencia que no evalua 
  si es mayuscula o minuscula
*/

$donde = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

$que_busco = "aliquip ex ea commodo";
$que_busco_M = "ALIQUIP EX EA COMMODO";

echo strstr($donde, $que_busco)."1";
echo "<br>";
echo strstr($donde, $que_busco_M)."2";
echo "<br>";
echo stristr($donde, $que_busco_M)."3";