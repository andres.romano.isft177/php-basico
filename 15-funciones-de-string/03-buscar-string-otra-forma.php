<?php 
/*
  Buscar con strpos() - stripos, en este caso encuentra la 
  posición numérica de la primera coincidencia del string y
  si no lo encuentra devuelve falso
*/
$donde = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

$que_busco = "aliquip ex ea commodo";
$que_busco_M = "ALIQUIP EX EA COMMODO";
$otro = "Andres";

echo strpos($donde, $que_busco);
echo "<br>";
echo stripos($donde, $que_busco_M);
echo "<br>";
var_dump(stripos($donde, $otro));
echo "<br>";
echo strlen("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ");
echo "<br>";
echo stripos($donde, $otro) ? "Se encontro el string" : "No se encontro el string";