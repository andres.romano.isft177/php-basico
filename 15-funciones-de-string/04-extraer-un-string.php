<?php 
/*
  Extraer un cadena con substr(string, start, length), devuelve una parte del 
  string definida por los parámetros start y length, con numeros negativos
  se empieza por el final de la cadena.
*/

$string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
# texto completo
echo $string;
echo "<br><br>";
# los primeros 20 caracteres
echo substr($string, 0, 20);
echo "<br><br>";
# todo el texto, menos, los ultimos 20 caracteres
echo substr($string, 0, -20);
echo "<br><br>";
# los ultimos 20 caracteres
echo substr($string, -20, 20);
echo "<br><br>";