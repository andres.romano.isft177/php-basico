<?php 
/*
  Para separa un string usamos explode() al que hay que pasarle
  un separador y un string y me devuelve un array. Para juntar 
  tengo utilizar implode() y se le debe para un separador y un
  array y me devuelve un string
*/

$explode = "Andres|Dario|Romano";
$implode = ["Andres","Dario","Romano"];

var_dump(explode("|", $explode));
echo "<br><br>";
var_dump(implode(" ", $implode));
