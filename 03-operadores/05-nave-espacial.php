<?php
/*
Operador nave espacial

Un integer menor que, igual a, o mayor que cero cuando $a es respectivamente menor que, igual a, o mayor que $b. 
*/
echo 1 <=> 1;
echo "<br>";
echo 1 <=> 2;
echo "<br>";
echo 2 <=> 1;
echo "<br>";
echo "<br>";
echo 1.5 <=> 1.5;
echo "<br>";
echo 1.5 <=> 2.1;
echo "<br>";
echo 2.1 <=> 1.5;
echo "<br>";
echo "<br>";
echo "1" <=> "1";
echo "<br>";
echo "1" <=> "2";
echo "<br>";
echo "2" <=> "1";
echo "<br>";
echo "<br>";
