<?php
# Operadores aritmeticos y de asignacion

$a = 6;
echo -$a ** 3; // el doble * significa exponente
echo "<br>";
echo $a ** 3; // se puede poner con o sin signo
echo "<br>";

$a = 30;
$a = $a + 5 / 2 * 10 - 5;
echo $a;
echo "<br>";
$a += 10;
echo $a;
echo "<br>";
$a /= 2;
echo $a;
echo "<br>";
$a *= 1.5;
echo $a;
echo "<br>";

# incrementos y decrementos

$a = 10;

echo $a++; // incremento
echo "<br>";
echo $a--; // decremento
echo "<br>";

# precedencia

echo ++$a; // incremento
echo "<br>";
echo --$a; // decremento
echo "<br>";
