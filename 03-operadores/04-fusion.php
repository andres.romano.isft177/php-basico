<?php
/*
Operador de fusion de null
Devuelve su primer operando si existe y no es NULL; de lo contrario devuelve su segundo operando.
En particular, este operador no emite un aviso o advertencia si el valor del lado izquierdo no existe, al igual que isset(). Esto es especialmente útil en claves de arrays.
*/

$val = null;
$var = "var";

echo $val ?? $var; // va a mostrar var

echo "<br>";

$usuario = $_GET["usuario"] ?? $_POST["usuario"] ?? "Anonimo"; // va a mostrar Anonimo

echo $usuario;

echo "<br>";

$foo = null;
$baz = 1;
$qux = 2;

echo $foo ?? $bar ?? $baz ?? $qux; // va a mostrar 1
