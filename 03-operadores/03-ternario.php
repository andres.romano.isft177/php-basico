<?php 
/*
El operador Ternario:

La expresión (expr1) ? (expr2) : (expr3) evalúa a expr2 si expr1 se evalúa como true y a expr3 si expr1 se evalúa como false.
*/

# ejemplo para asignar un valor

$value = 10;

$resultado = $value >= 5 ? "verdadero" : "falso";

echo $resultado;

echo "<br>";

# ejemplo con una funcion

$resultado = is_string($value) ? "value es string" : "value no es string";

echo $resultado;

if ($value >= 5) {
	echo "verdadero"; 
} else {
	echo "falso"; 
}