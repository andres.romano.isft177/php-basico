<?php 
/*
Los operadores de comparación, como su nombre lo indica, permiten comparar dos valores:
$a == $b Igual, TRUE si $a es igual a $b después de la manipulación de tipos.
$a === $b Idéntico, TRUE si $a es igual a $b, y son del mismo tipo.
$a != $b Diferente, TRUE si $a no es igual a $b después de la manipulación de tipos.
$a <> $b Diferente TRUE, si $a no es igual a $b después de la manipulación de tipos.
$a !== $b No idéntico, TRUE si $a no es igual a $b, o si no son del mismo tipo.
$a < $b Menor que, TRUE si $a es estrictamente menor que $b.
$a > $b Mayor que, TRUE si $a es estrictamente mayor que $b.
$a <= $b Menor o igual que, TRUE, si $a es menor o igual que $b.
$a >= $b Mayor o igual que, TRUE, si $a es mayor o igual que $b.
*/

$a = 18;
$b = 18.1;
if($a !== $b){

	echo "la condición es verdadera";

} else {

	echo "la condición es falsa";

}