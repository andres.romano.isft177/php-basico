<?php
# cadenas de texto

$materia = "Algoritmos 2";
$curso = '2do';
$dia = "Viernes";

#print $materia;
#print $curso;
#print $dia;

# Heredoc: evalua los valores de las variables
$str = <<<EOD
Materia: $materia <br>
Curso: $curso <br>
Día: $dia <br>
<br>
EOD;

# NowDoc: no evalua los valores de las variables, se puede usar para mostrar codigo
$str2 = <<<'EOD'
Materia: $materia <br>
Curso: $curso <br>
Día: $dia <br>
<br>
EOD;

print $str;
echo $str2;

# Concatenar

$salida = "Materia: ".$materia."<br>";
$salida .= "Curso: ".$curso."<br>";
$salida .= "Día: ".$dia;
print $salida."\n";

