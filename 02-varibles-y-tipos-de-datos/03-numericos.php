<?php
$entero = 12345678;
var_dump($entero);
echo "<br>";

$negativo = -12345678;
var_dump($negativo);
echo "<br>";

$float = 123.45678;
var_dump($float);
echo "<br>";

$octal = 0012345; //octal 0-7
var_dump($octal);
echo "<br>";

$hexadecimal = 0xFFAADD; //hexadecimal 0-9 a-f
var_dump($hexadecimal);
echo "<br>";

$binario = 0b1010101; //Binario 0-1
var_dump($binario);
echo "<br>";

