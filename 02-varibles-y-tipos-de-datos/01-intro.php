<?php
# Declaracion de variables
# Si
$valor1;

# No
#$1valor; // esto da error

# variables numericas
$entero = 1; // tipo entero, int
$decimal = "2.3"; // tipo decimal, float
var_dump($decimal);
$decimal = 2.3; // tipo decimal, float
var_dump($decimal);


print $entero."<br>";
print $decimal . "<br>";

# variables tipo string
$string = 'cadena de texto'." repetido";

echo $string."<br>";

echo $string." ".$decimal."<br>";

# variables booleanas

$verdadero = true;
$falso = false;

var_dump($verdadero);
echo "<br>";
var_dump($falso);

# tipo array

$array = array(); // declaro un array vacio
$array1 = []; // otra forma de declarar un array vacio

echo "<br>";
var_dump($array);
echo "<br>";
var_dump($array1);

$array_num = [1, 2.3, "elemento", 8 => "valor", 10 => "10"];
#$array_num = [1, 2.3, "elemento", "valor", "10"];
echo "<br>";
var_dump($array_num);

$array_assoc = [
   "materia" => "bases de datos", 
   "profe" => "andres romano", 
   "dia" => "miercoles"
];
echo "<br>";
var_dump($array_assoc);

# acceder a los datos de un array se especifica el indice
echo "<br>";
echo $array_assoc["materia"];
echo "<br>";
echo $array_num[8];
