<!DOCTYPE html>
<html>
<head>
	<title>Variables de variables y contantes</title>
	<meta charset="utf-8">
</head>
<body>
	<h3>Constantes</h3>
	<?php 
		define("DOLAR_BLUE", 935.53);
		$pesos = 20000;
		echo $pesos." $ equivalen a: ".round( ($pesos / DOLAR_BLUE), 2)." Dolares";
	?>
	<h3>Variables de variables</h3>
	<?php  
		$merlo = 580806;
		$ituzaingo = 180232;
		$ciudad = "merlo";
		echo "<p>La población de la ciudad $ciudad es de ${$ciudad}</p>";
		$ciudad = "ituzaingo";
		echo "<p>La población de la ciudad $ciudad es de ${$ciudad}</p>";
	?>
</body>
</html>