<?php
/**
 * Documentacion oficial de PHP 
 * 
 * @link https://www.php.net/manual/es/langref.php
 */

echo "Hola mundo"."\n";

// ... más código

#echo "<br>";

print "Última sentencia";

// el script finaliza aquí sin etiqueta de cierre de PHP

