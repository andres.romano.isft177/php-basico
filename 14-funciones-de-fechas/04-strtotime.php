<?php 
/**
 * Esta función espera que se proporcione una cadena que 
 * contenga un formato de fecha en Inglés US e intentará 
 * convertir ese formato a una fecha Unix
 * 
 * @link https://www.php.net/manual/es/function.strtotime.php
 */

echo strtotime("now"), " Ahora<br>";
echo strtotime("+1 day"), " un día más<br>";
echo strtotime("+1 week"), " una semana más<br>";
echo strtotime("+1 week 2 days 4 hours 2 seconds"), " una semana, 2 días, 4hr, 2s más<br>";
echo strtotime("next Thursday"), " proximo jueves<br>";
echo strtotime("last Monday"), " el lunes pasado<br>";