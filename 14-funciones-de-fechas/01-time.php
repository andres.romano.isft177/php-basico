<?php 
/**
 * La funcion time() devuelve la fecha UNIX actual, la fecha UNIX
 * empieza a contas segundos a partir del 1 de enero de 1970, podemos
 * sumar o restarle tiempo (seg*min*horas*dias), luego podemos formatearla
 * con la funcion date()
 * 
 * @link https://www.php.net/manual/es/function.time.php
 *  
 */

$now = time();

$ten_days = $now + (60*60*24*10);

echo "hora UNIX actual: ".$now."<br><br>";

echo "dentro de 10 dias: ".$ten_days;
