<?php 
/**
 * La funcion date() nos devuelve la fecha/hora actual, tambien
 * podemos formatearla y hacer operaciones con fechas, se le debe
 * pasar un string con cierto formato, leer documentacion, pero
 * usaremos el siguiente ejemplo este formato que es el standar
 * de MySQL, "Y-m-d H:i:s"
 * 
 * @link https://www.php.net/manual/es/function.date.php
 */

$today = date("Y-m-d H:i:s");

echo "fecha actual: ".$today."<br><br>";

# date() acepta un segundo parametro

$now = time();

$ten_days = $now + (60*60*24*10);

echo date("Y-m-d H:i:s", $ten_days);