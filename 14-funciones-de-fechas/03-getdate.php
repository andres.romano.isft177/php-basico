<?php 
/**
 * getdate() nos devuelve un array con la informacion de la fecha
 * y hora actual, tambien devuelve el equivalente en UNIX
 * 
 * array(11) { 
 * 	["seconds"]=> int(9) 
 * 	["minutes"]=> int(37) 
 * 	["hours"]=> int(16) 
 * 	["mday"]=> int(26)
 * 	["wday"]=> int(2) 
 * 	["mon"]=> int(3) 
 * 	["year"]=> int(2024) 
 * 	["yday"]=> int(85) 
 * 	["weekday"]=> string(7) "Tuesday" 
 * 	["month"]=> string(5) "March" 
 * 	[0]=> int(1711471029) 
 * }
 * 
 * @link https://www.php.net/manual/es/function.time.php
*/
 

$date = getdate();

var_dump($date);