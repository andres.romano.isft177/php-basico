<?php 
/**
 * El método utilizado por PHP para tratar datos JSON es simplemente
 * convertir cadenas (string) en formato JSON a arreglos de PHP. 
 * Para ello se utiliza la función json_decode(string) la cual recibe
 * dicha cadena y retorna en arreglo.
 * 
 * @link https://www.php.net/manual/es/ref.json.php
 */

# leer un archivo JSON

$dir = "../_files/"; // directorio que usaremos de practica
$file = "usuarios.json"; // nombre del archivo

$json_obj = json_decode(file_get_contents($dir.$file));
$json_arr = json_decode(file_get_contents($dir.$file),JSON_OBJECT_AS_ARRAY);

#/*
var_dump($json_obj);
echo "<br><br>";
var_dump($json_arr);
#*/

/*
echo "<h3>Acceder a datos de json_obj</h3>";

for ($i=0; $i < count($json_obj); $i++) { 
	echo "Nombre: ".$json_obj[$i]->nombre."<br>";
	echo "Apellidos: ".$json_obj[$i]->apellidos."<br>";
	echo "Edad: ".$json_obj[$i]->edad."<br>";
	echo "Municipio: ".$json_obj[$i]->ubicacion->municipio."<br>";
	echo "Localidad: ".$json_obj[$i]->ubicacion->localidad."<br>";
	echo "Calle: ".$json_obj[$i]->ubicacion->calle."<br>";
	echo $json_obj[$i]->alta ? "Alta: activado"."<br>" : "Alta: desactivado"."<br>";
	echo "<br>";
}
*/

/*
echo "<h3>Acceder a datos de json_arr</h3>";

for ($i=0; $i < count($json_arr); $i++) { 
	
	$keys = array_keys($json_arr[$i]);

	for ($j=0; $j < count($keys); $j++) {

		if ($keys[$j] == "alta") {
			echo $json_arr[$i][$keys[$j]] ? ucfirst($keys[$j]).": activado<br>" : ucfirst($keys[$j]).": desactivado<br>";
			continue;
		}
		if (is_array($json_arr[$i][$keys[$j]])) {
			echo ucfirst($keys[$j]).": <br>";
			$keys_k = array_keys($json_arr[$i][$keys[$j]]);

			for ($k=0; $k < count($keys_k); $k++) { 
				echo "&emsp;".ucfirst($keys_k[$k]).": ".$json_arr[$i][$keys[$j]][$keys_k[$k]]."<br>";
			}

			continue;
		}

		echo ucfirst($keys[$j]).": ".$json_arr[$i][$keys[$j]]."<br>";

	}
	echo "<br>";
}
*/