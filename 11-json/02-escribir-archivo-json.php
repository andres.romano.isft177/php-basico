<?php 
/*
De igual forma para escribir datos en formato JSON, PHP 
utiliza la función json_encode(array) la cual recibe un 
arreglo de PHP y retorna una hilera de texto en formato JSON.
*/

$dir = "../_files/"; // directorio que usaremos de practica
$file = "items.json"; // nombre del archivo

$items = [
	"nombre" => "Memoria RAM",
	"tipo" => "SODIMM",
	"modelo" => "DDR3",
	"marca" => "Kingstone",
	"caracteristicas" => [
		"producto nuevo",
		"blister sellado",
		"velocidad 1666 Mhz"
	],
	"precio" => 1200,
	"stock" => true
];

# sacar por pantalla

echo json_encode($items);

# generar un archivo

if (!file_exists($dir.$file)) {

	touch($dir.$file);
	chmod($dir.$file, 0777);

	$json = fopen($dir.$file, "w");

	fwrite($json, json_encode($items,JSON_PRETTY_PRINT));

	fclose($json);

}