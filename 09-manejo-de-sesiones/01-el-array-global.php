<?php
session_start();
/*
Para manejar sesiones utilizaremos el array global $_SESSION que esta 
disponible en todo nuestro entorno de PHP y podemos accede a el siempre 
y cuando este al inicio del script la funcion session_start().
el array $_SESSION podemos llenarlo con los datos que necesitemos
creando los indices y asignandoles valores

*/

$_SESSION['id'] = 1;
$_SESSION['rol'] = 2;
$_SESSION['user'] = "aromano";
$_SESSION['pass'] = "123";
