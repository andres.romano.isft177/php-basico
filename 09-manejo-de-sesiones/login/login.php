<?php 
session_start(); 
if (isset($_POST['submit'])) {
	
	if ( (strcmp($_SESSION['user'], $_POST['user']) == 0) && (strcmp($_SESSION['pass'], $_POST['pass']) == 0) ) {
		header('Location:inicio.php');
	} else {
		echo '<script>
		alert("Usuario o contraseña incorrecta");
		</script>';
	}
	
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="form.css">
	<title>login</title>
</head>
<body>
	<form action="" method="post">
		<label for="user">user</label>
		<input type="text" name="user">
		<br><br>
		<label for="pass">pass</label>
		<input type="password" name="pass">
		<br><br>
		<input type="submit" name="submit" value="Ingresar">
	</form>	
</body>
</html>
