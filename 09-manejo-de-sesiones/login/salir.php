<?php 
session_start();

if (isset($_SESSION['id'])) {
	session_destroy();
	unset($_SESSION['id']);
	unset($_SESSION['rol']);
	unset($_SESSION['user']);
	unset($_SESSION['pass']);
}

header('Location:login.php');
