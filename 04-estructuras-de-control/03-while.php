<?php
/*
En el ciclo “while” primero se hace la pregunta, y si es verdadera, se ejecuta un bloque de
sentencias y se vuelve a repetir la pregunta, hasta que ésta resulte falsa.

while (condition) {
	// code...
}
*/
$contador = 1;
$veces = 10;
$x = true;

while ($contador <= $veces) {
	echo "contador = ".$contador;
	echo "<br>";
	$contador ++;
}
echo "<br>";
while ($x) {
	$contador ++;
	if ($contador == 15) {
		echo "contador = ".$contador;
		echo "<br>";
		$x = false;
	}
}

var_dump($contador);
echo "<br>";
var_dump($veces);
echo "<br>";
var_dump($x);