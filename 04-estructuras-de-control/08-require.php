<?php
/*
require es idéntico a include excepto que en caso de fallo producirá 
un error fatal y detiene el script
*/

echo "hacemos require del archivo de 01-if-else.php";
echo "<br><br>";

require '01-if-else.php';

/* require_once, si el código del fichero ya ha sido incluido, 
no se volverá a incluir
*/

require_once '01-if-else.php';
require_once '02-switch.php';
require_once '2-switch.php';

echo "aca seguiria el codigo";
