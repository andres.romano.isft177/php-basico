<?php 
/*
if else

Recomendado en archivos solo con codigo PHP

if (condition) {
	// code...
}

Recomendado para archivos que contienen PHP y HTML

if (condition) :
	// code...
endif

if elseif ...

if (condition) {
	// code...
} elseif (condition) {
	
} else {
	
}
*/
$var = 2;

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Condicionales</title>
</head>
<body>
	<h2>if else</h2>
	<?php if ($var < 5): ?>
		<h3>$var es menor a 5</h3>
	<?php else: ?>
		<h3>$var es mayor a 5</h3>
	<?php endif ?>
	<hr>
	<h2>if elseif</h2>
	<?php if ($var < 5): ?>
		<h3>$var puede ser 1,2,3 ó 4</h3>
	<?php elseif ($var > 5): ?>
		<h3>$var puede ser 6,7,8 ...</h3>
	<?php else: ?>
		<h3>$var puede es 5</h3>
	<?php endif ?>
</body>
</html>