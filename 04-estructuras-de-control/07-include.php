<?php 
/*
La sentencia include incluye y evalúa el archivo especificado.
como ejemplo incluiremos un archivo ya visto
*/

echo "incluiremos el archivo de 01-if-else.php";
echo "<br><br>";

include '01-if-else.php';

/* include_once, si el código del fichero ya ha sido incluido, 
no se volverá a incluir
*/

include_once '01-if-else.php';
include_once '02-switch.php';
include_once '2-switch.php';

echo "aca seguiria el codigo";
