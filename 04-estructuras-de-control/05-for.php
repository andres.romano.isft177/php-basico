<?php 
/*
En los ejemplos anteriores, es muy común usar una variable "contador", la
cual es inicializada en algún valor, el ciclo for ya cuenta con esa variable de control

for ($i=0; $i < condition ; $i++) { 
	// code...
}
*/
$veces = 10;
$array = [2,4,"rojo","amarillo","verde",10];

for ($i=0; $i < $veces ; $i++) { 
	echo "ciclo n°: ".$i;
	echo "<br>";
}

echo "<br>";

for ($i=0; $i < count($array) ; $i++) { 
	echo $array[$i];
	echo "<br>";
}