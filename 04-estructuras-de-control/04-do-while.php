<?php
/*
La sentencia do..while() primero se hace el bloque de sentencias y luego la expresión
condicional, quiere decir que al menos una vez se va a ejecutar el codigo sin evaluar
la condición

do {
	// code...
} while (condition);
*/
$contador = 1;
$veces = 10;
$x = true;

do {
	echo "contador = ".$contador;
	echo "<br>";
	$contador ++;
} while ($contador <= $veces);

echo "<br>";

do {
	echo "contador = ".$contador;
	echo "<br>";
	$x = false;
} while ($x);
