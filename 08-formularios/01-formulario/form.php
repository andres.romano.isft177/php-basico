<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="form.css">
	<title>Formulario</title>
</head>
<body>
	<form action="script.php" method="post">
		<label for="nombre">nombre</label>
		<input type="text" name="nombre">
		<br>
		<label for="email">email</label>
		<input type="email" name="email">
		<br>
		<label for="edad">edad</label>
		<input type="number" name="edad">
		<br>
		<label for="sex">M</label>
		<input type="radio" name="sex" value="M">
		<label for="sex">F</label>
		<input type="radio" name="sex" value="F">
		<br>
		<label for="fechaNacimiento">fecha nacimiento</label>
		<input type="date" name="fechaNacimiento">
		<br>
		<label for="localidad">localidad</label>
		<select name="localidad">
			<option>-</option>
			<option value="1">Merlo</option>
			<option value="2">Libertad</option>
			<option value="2">Padua</option>
		</select>
		<br>
		<label for="messege">Mensaje</label><br>
		<textarea name="messege" rows="6"></textarea><br>
		<label for="ok">estoy de acuerdo</label>
		<input type="checkbox" name="ok" value="ok">
		<br>
		<input type="submit" name="submit" value="Enviar">
	</form>	
</body>
</html>
