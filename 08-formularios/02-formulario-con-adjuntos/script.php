<?php 
/**
 * Para saber que ocurre con los archivos se debe manejar con 
 * el valor del indice, [error] => 0, que en integer.
 * con el indice [type] validamos el tipo de archivo (pdf, jpg, etc)
 * con el indice [size] validamos el tamaño del archivo (se expresa en KB, 
 * tener en cuenta que es un string, hay que convertirlo a int con la funcion intval())
 * con el indice [tmp_name] se sube el archivo
 * con el indice [name] podemos usar el nombre original del archivo
 * 
 * @link https://www.php.net/manual/en/features.file-upload.errors.php
 * 
 * Extensiones MIME para validar tipos de archivo, indice [type]
 * 
 * @link https://developer.mozilla.org/es/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
 */

print_r($_FILES);

echo "<br>";

$uploaddir = "/var/www/html/php-basico/_files/";

$uploadfile = $uploaddir.$_FILES['userfile']['name'];

if ( ($_FILES['userfile']['error'] === 0)
	&& (intval($_FILES['userfile']['size']) <= 2000000)
	&& ($_FILES['userfile']['type'] === "application/pdf" )
	) {
# si el archivo cumple se sube
#	move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)
    echo "El archivo se subio correctamente";

} else {

    echo "Error al subir el archivo, no cumple con los requisitos";

}
