<?php 
/**
 * las validaciones HTML se pueden hacer con expresiones regulares (RegExp)
 * usando el atributo pattern, tambien hay validaciones incorporadas, ambas
 * se escriben en el HTML
 * 
 * @link https://developer.mozilla.org/es/docs/Learn/Forms/Form_validation
 * 
 * las validaciones del lado del servidor las hacemos con funciones PHP, la 
 * funcion preg_math() utiliza tambien expresiones regulares (RegExp) y es 
 * la manera mas precisa de validar datos.
 * 
 * @link https://www.php.net/manual/en/function.preg-match
 * 
 * igualmente les dejo un link con 3 formas de validar datos para que lean
 * @link https://diego.com.es/filtrado-de-datos-de-entrada-en-php
 * 
 */

# si el input es "required" no puede venir vacio, utilizamos

if ( (isset($_POST['nombre'])) && (!empty(trim($_POST['nombre']))) ) {
	if ((preg_match('/^[A-Za-z ]+$/', $_POST['nombre'])) 
		&& (strlen($_POST['nombre']) >= 6) 
		&& (strlen($_POST['nombre']) <= 10)) {

		echo $_POST['nombre']." es valido <br>";

	} else {

		echo $_POST['nombre']." NO es valido <br>";

	}
} else {

	echo "nombre es requerido y no puede estar vacio <br>";

}

if ( (isset($_POST['edad'])) && (!empty(trim($_POST['edad']))) ) {
	if ((preg_match('/^[0-9]+$/', $_POST['edad'])) 
		&& ($_POST['edad'] >= 0) 
		&& ($_POST['edad'] <= 90)) {

		echo $_POST['edad']." es valido <br>";

	} else {

		echo $_POST['edad']." NO es valido <br>";

	}
} else {

	echo "edad es requerido y no puede estar vacio <br>";

}
